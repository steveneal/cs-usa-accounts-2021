package com.creditsuisse.account;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AccountTest {

    @Test
    public void givenZeroBalanceEqualDepositAndWithdrawGivesZeroBalance() {
        Account a = new Account();
        assertEquals(0, a.getBalance());
        a.deposit(100);
        a.withdraw(100);
        assertEquals(0, a.getBalance());
    }

    @Test
    public void givenZeroBalanceWithdrawResultsInNegativeBalance() {
        Account a = new Account();
        assertEquals(0, a.getBalance());
        a.withdraw(100);
        assertTrue(a.getBalance() < 0);
    }

    @Test
    public void negativeDepositsNotAllowed() {
        assertThrows(IllegalArgumentException.class, () -> {
            Account a = new Account();
            a.deposit(-100);
        });
    }

    @Test
    public void negativeWithdrawalNotAllowed() {
        assertThrows(IllegalArgumentException.class, () -> {
            Account a = new Account();
            a.withdraw(-100);
        });
    }

}

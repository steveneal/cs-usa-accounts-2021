package com.creditsuisse.account;

import org.junit.jupiter.api.Test;

import java.util.Currency;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckingAccountTest {

    @Test
    public void check2PcFeeApplied() {
        CheckingAccount account = new CheckingAccount();
        account.deposit(200_00);
        account.withdraw(100_00);//should apply 2% fee of $2
        assertEquals(98_00, account.getBalance());
    }

    @Test
    public void check2PcFeeAccumulated() {
        CheckingAccount account = new CheckingAccount();
        account.deposit(200_00);
        account.withdraw(100_00);//should apply 2% fee of $2
        assertEquals(98_00, account.getBalance());
    }

    @Test
    public void checkFeeRoundedUp() {
        CheckingAccount account = new CheckingAccount();
        account.deposit(200_00);
        account.withdraw(99_00);//should apply 2% fee of $1.98
        assertEquals(99_02, account.getBalance());
    }

    @Test
    public void checkFeeRoundedDown() {
        CheckingAccount account = new CheckingAccount();
        account.deposit(200_00);
        account.withdraw(55_00);//should apply 2% fee of $1.10
        assertEquals(143_90, account.getBalance());
    }

    @Test
    public void check1DollarFeeAppliedForSmallWithdraw() {
        CheckingAccount account = new CheckingAccount();
        account.deposit(200_00);
        account.withdraw(10_00);//fee will be under $1 and should be rounded up to $1
        assertEquals(189_00, account.getBalance());
    }

    @Test
    public void testDepositEur() {
        CheckingAccount account = new CheckingAccount();
        account.deposit(100, Currency.getInstance("EUR"));
        assertEquals(100 * CurrencyAccount.EUR_RATE, account.getBalance(), 0.00001);
    }

    @Test
    public void testDepositGbp() {
        CheckingAccount account = new CheckingAccount();
        account.deposit(100, Currency.getInstance("GBP"));
        assertEquals(100 * CurrencyAccount.GBP_RATE, account.getBalance(), 0.00001);
    }

}

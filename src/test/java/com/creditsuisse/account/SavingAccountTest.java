package com.creditsuisse.account;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class SavingAccountTest {

    @Test
    public void cannotBecomeOverdrawn() {
        assertThrows(BankingException.class, () -> {
            SavingAccount a = new SavingAccount();
            a.withdraw(1);
        });
    }
}

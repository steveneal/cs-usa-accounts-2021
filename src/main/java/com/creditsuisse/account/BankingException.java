package com.creditsuisse.account;

public class BankingException extends RuntimeException {
    public BankingException(String message) {
        super(message);
    }
}

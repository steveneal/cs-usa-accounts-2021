package com.creditsuisse.account;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Account regularAccount = new SavingAccount();
        CheckingAccount checkingAccount = new CheckingAccount();

        List<Account> accounts = new ArrayList<>();

        accounts.add(regularAccount);
        accounts.add(checkingAccount);

        List<Integer> balances = accounts.stream()
                .peek(a -> a.deposit(100_00))
                .peek(a -> a.withdraw(50_00))
                .map(a -> a.getBalance())
                .collect(Collectors.toList());

        System.out.println(balances);

    }
}

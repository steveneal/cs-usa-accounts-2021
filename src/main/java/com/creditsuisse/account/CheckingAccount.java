package com.creditsuisse.account;

import java.util.Currency;

public class CheckingAccount extends Account implements CurrencyAccount {

    public static final double RATE = 0.02;

    @Override
    public void withdraw(int amount) {
        int fee = (int) Math.round(amount * RATE);
        if (fee < 1_00) {
            fee = 1_00;
        }
        super.withdraw(amount + fee);
    }

    @Override
    public void deposit(int amount, Currency currency) {
        if (currency.getCurrencyCode().equals("EUR")) {
            amount *= EUR_RATE;
        }
        if (currency.getCurrencyCode().equals("GBP")) {
            amount *= GBP_RATE;
        }
        deposit(amount);
    }
}

package com.creditsuisse.account;

import java.util.Currency;

public interface CurrencyAccount {

    public static final double EUR_RATE = 0.89;
    public static final double GBP_RATE = 0.77;

    void deposit(int amount, Currency currency);
}

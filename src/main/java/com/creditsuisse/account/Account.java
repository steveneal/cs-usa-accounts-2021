package com.creditsuisse.account;

public class Account {
    private int balance;

    public void deposit(int amount) {
        assertPositive(amount);
        balance += amount;
    }

    public void withdraw(int amount) {
        assertPositive(amount);
        balance -= amount;
    }

    public int getBalance() {
        return balance;
    }

    private static void assertPositive(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("negative amounts are not permitted");
        }
    }

    @Override
    public String toString() {
        return "Account{" +
                "balance=" + balance +
                '}';
    }
}

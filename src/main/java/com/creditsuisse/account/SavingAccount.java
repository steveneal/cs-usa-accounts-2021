package com.creditsuisse.account;

public class SavingAccount extends Account {

    @Override
    public void withdraw(int amount) {
        if (amount <= getBalance()) {
            super.withdraw(amount);
        } else {
            throw new BankingException("insufficient funds");
        }
    }
}
